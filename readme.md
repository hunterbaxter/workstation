mostly functional workstation configuration

```sh
git clone --recurse-submodules -j8 https://gitlab.com/hunterbaxter/workstation.git
```

# TODO:

- password manager
- bookmark manager
- screenshot on i3 terrible compared to wayland
- **lots** of nvim fixes
- neomutt
